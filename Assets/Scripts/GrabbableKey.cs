﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabbableKey : MonoBehaviour
{
    [SerializeField]
    private GrabbableObjectSO m_grabbableObjectSO;
    [SerializeField]
    private PlayerSO m_playerSO;

    private PlayerData m_data;

    void Awake()
    {
        m_data = GameObject.FindGameObjectsWithTag("Player")[0].GetComponent<PlayerData>();
    }

    public void GrabObject()
    {
        m_data.hasKey = true;
        Destroy(gameObject);
    }
}
