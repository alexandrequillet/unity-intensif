﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundSoundScript : MonoBehaviour
{
    private static BackgroundSoundScript instance = null;

    public static BackgroundSoundScript Instance
    {
        get
        {
            return instance;
        }
    }

    void Start()
    {
        
    }
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
                return;
        }
        else
        {
            instance = this;
        }

        DontDestroyOnLoad(this.gameObject);
    }
}
