﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crawler : MonoBehaviour
{
    [SerializeField]
    private Transform player;
    [SerializeField]
    private GameObject door;
    [SerializeField]
    private float speed, range;
    [SerializeField]
    private PlayerData m_data;
    private bool inRange { get { return Vector3.Distance(transform.position, player.position) <= range; } }
    private Animator animator;

    private bool fearIncreased = false;

    [SerializeField] private PlayerSO m_playerSO;

    void Awake()
    {
        animator = GetComponent<Animator>();
        m_data = GameObject.FindGameObjectsWithTag("Player")[0].GetComponent<PlayerData>();
    }

    // Update is called once per frame
    void Update()
    {
        attack();
    }

    void attack()
    {
        Vector3 offset = new Vector3(0, 90, 0);
        if (inRange)
        {
            if (m_data.hasSword)
            {
                kill();
            }
            animator.SetBool("attack", true);
            transform.LookAt(player);
            transform.Rotate(offset);

            if (!fearIncreased)
            {
                m_playerSO.fearSpeed /= 2;
                fearIncreased = true;
            }

            else
            {
                animator.SetBool("attack", false);
                transform.LookAt(new Vector3(0, 0, -1));
                if (fearIncreased)
                {
                    m_playerSO.fearSpeed *= 2;
                    fearIncreased = false;
                }
            }
        }

    }

    public void kill()
    {
        door.GetComponent<DoorController>().openDoor();
        Destroy(gameObject);
    }
}
