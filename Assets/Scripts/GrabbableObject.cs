﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabbableObject : MonoBehaviour
{
    [SerializeField]
    private GrabbableObjectSO m_grabbableObjectSO;
    [SerializeField]
    private PlayerSO m_playerSO;

    private PlayerData m_data;

    void Awake()
    {
        m_data = GameObject.FindGameObjectsWithTag("Player")[0].GetComponent<PlayerData>();
    }

    public void GrabObject()
    {
        m_data.fear -= m_grabbableObjectSO.removeFear;
        m_data.score += m_grabbableObjectSO.addScore;
        Destroy(gameObject);
    }
}
