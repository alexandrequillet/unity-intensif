﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class FearProgression : MonoBehaviour
{
    [SerializeField]
    private TicTac m_FearClock;

    [SerializeField]
    private PlayerSO m_playerSO;

    [SerializeField]
    private Slider m_slider;
    [SerializeField]
    private Text m_sliderText;

    private PlayerData m_data;

    void Awake()
    {
        m_data = GameObject.FindGameObjectsWithTag("Player")[0].GetComponent<PlayerData>();
    }

    void Start()
    {
        m_FearClock.fearProgress.AddListener(FearProgress);
        m_slider.value = m_data.fear;
        m_sliderText.text = "Fear : " + m_data.fear  + "% ";
    }

    void FearProgress()
    {
        m_data.fear++;
        if (m_data.fear >= 100)
        {
            m_data.fear = 100;
        }
        m_slider.value = m_data.fear / 100f;
        m_sliderText.text = "Fear : " + m_data.fear + "% ";
    }
}
