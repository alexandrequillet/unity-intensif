﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameOverSystem : MonoBehaviour
{
    private PlayerData m_data;

    void Awake()
    {
        m_data = GameObject.FindGameObjectsWithTag("Player")[0].GetComponent<PlayerData>();
    }
    void Update()
    {
        if (m_data.fear == 100)
        {
            SceneManager.LoadScene("GameOverScene");
            Time.timeScale = 0;
        }
      
    }
}
