﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float speed = 5f;

    [SerializeField] private KeySO keys;

    [SerializeField] private CharacterController controller;
    

    // Update is called once per frame
    void Update()
    {
        float x = 0f;
        float z = 0f;
        if (Input.GetKey(keys.moveLeftKey))
        {
            x--;
        }
        if (Input.GetKey(keys.moveRightKey))
        {
            x++;
        }
        if (Input.GetKey(keys.moveForwardKey))
        {
            z++;
        }
        if (Input.GetKey(keys.moveBackwardKey))
        {
            z--;
        }

        Vector3 move = transform.right * x + transform.forward * z;

        controller.Move(move * speed * Time.deltaTime);
    }
}
