﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public Text highScoreText;
    public int highScoreCount;
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("highscore"))
        {
            highScoreCount = PlayerPrefs.GetInt("highscore");
        }

        highScoreCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        SetScoreText();
    }    
    public void SetScoreText()
    {
        highScoreText.text = "Score : " + highScoreCount;
        
    }
}
