﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    [SerializeField]
    private float openDuration = 2f;
    [SerializeField]
    private Vector3 targetRotation;
    private bool isOpen = false;

    public void openDoor()
    {
        if (!isOpen)
        {
            StartCoroutine(DoorCoroutine());
            isOpen = true;
        }
    }

    IEnumerator DoorCoroutine()
    {
        float t = 0;
        while (t < 1)
        {
            t += Time.deltaTime / openDuration;
            transform.eulerAngles = Vector3.Slerp(transform.eulerAngles, targetRotation, t);
            yield return null;
        }
    }
}
