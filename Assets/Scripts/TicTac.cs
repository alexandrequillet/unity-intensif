﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TicTac : MonoBehaviour
{
    public UnityEvent fearProgress;

    [SerializeField]
    private PlayerSO m_playerSO;

    void Awake()
    {
        fearProgress = new UnityEvent();
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(FearProgress());
    }

    IEnumerator FearProgress()
    {
        while(true)
        {
            yield return new WaitForSeconds(m_playerSO.fearSpeed);
            fearProgress.Invoke();
        }
    }
}
