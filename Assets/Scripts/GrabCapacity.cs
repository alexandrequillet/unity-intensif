﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class GrabCapacity : MonoBehaviour
{

    [SerializeField]
    private GameObject m_grabText;
    [SerializeField]
    private Text m_fearText;
    [SerializeField]
    private Text m_scoreText;
    [SerializeField]
    private Text m_keyText;

    [SerializeField] private KeySO keys;

    [SerializeField]
    private float m_grabDistance = 5f;

    [SerializeField]
    private LayerMask m_objectMask;
    [SerializeField]
    private LayerMask m_keyMask;
    [SerializeField]
    private LayerMask m_swordMask;

    private PlayerData m_data;

    void Awake()
    {
        m_data = GameObject.FindGameObjectsWithTag("Player")[0].GetComponent<PlayerData>();
    }

    void Start()
    {
        m_grabText.SetActive(false);
    }

    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, m_grabDistance, m_objectMask))
        {
            if(Input.GetKeyDown(keys.grabKey))
            {
                GrabbableObject grabbed = hit.transform.parent.gameObject.GetComponent<GrabbableObject>();
                grabbed.GrabObject();
                UpdateScreen();
            }
            m_grabText.SetActive(true);
        }
        else
        {
            m_grabText.SetActive(false);
        }

        if (Physics.Raycast(ray, out hit, m_grabDistance, m_keyMask))
        {
            if (Input.GetKeyDown(keys.grabKey))
            {
                GrabbableKey grabbed = hit.transform.parent.gameObject.GetComponent<GrabbableKey>();
                grabbed.GrabObject();
                UpdateScreen();
            }
            m_grabText.SetActive(true);
        }
        else
        {
            m_grabText.SetActive(false);
        }

        if (Physics.Raycast(ray, out hit, m_grabDistance, m_swordMask))
        {
            if (Input.GetKeyDown(keys.grabKey))
            {
                GrabbableSword grabbed = hit.transform.parent.gameObject.GetComponent<GrabbableSword>();
                grabbed.GrabObject();
                UpdateScreen();
            }
            m_grabText.SetActive(true);
        }
        else
        {
            m_grabText.SetActive(false);
        }
    }

    private void UpdateScreen()
    {
        m_fearText.text = "Fear : " + m_data.fear + "%";
        m_scoreText.text = "Score : " + m_data.score;
        string key = "";
        
        if(m_data.hasKey)
        {
            key = "Key here / ";
        }
        else
        {
            key = "No Key / ";
        }
        if (m_data.hasSword)
        {
            key += "Sword here ";
        }
        else
        {
            key += "No Sword";
        }
        m_keyText.text = key;
    }
}
