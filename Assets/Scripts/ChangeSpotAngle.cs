﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSpotAngle : MonoBehaviour
{
    private PlayerData m_data;

    [SerializeField] private Light lightParams;

    void Awake()
    {
        m_data = GameObject.FindGameObjectsWithTag("Player")[0].GetComponent<PlayerData>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float fear = m_data.fear;
        lightParams.spotAngle = 140f * (100f - fear) / 100f +40f;
    }
}
