﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour
{
    public int fear = 20;
    public int score = 0;
    public bool hasKey = false;
    public bool hasSword = false;
}
