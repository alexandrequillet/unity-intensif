﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseSystem : MonoBehaviour
{
    private bool m_paused = false;

    [SerializeField]
    private GameObject m_pauseCanvas;
    [SerializeField]
    private GameObject m_mainCanvas;

    [SerializeField]
    private KeySO m_keySO;
    
    void Update()
    {
        if (Input.GetKeyDown(m_keySO.pauseKey))
        {
            if(m_paused)
            {
                Time.timeScale = 1;
                m_pauseCanvas.SetActive(false);
                m_mainCanvas.SetActive(true);
                m_paused = false;
            } 
            else
            {
                Time.timeScale = 0;
                m_pauseCanvas.SetActive(true);
                m_mainCanvas.SetActive(false);
                m_paused = true;
            }
        }         
    }
}
