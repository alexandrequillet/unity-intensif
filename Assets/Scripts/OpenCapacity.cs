﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class OpenCapacity : MonoBehaviour
{

    [SerializeField]
    private GameObject m_openText;

    [SerializeField] private KeySO keys;

    [SerializeField]
    private float m_openDistance = 5f;

    [SerializeField]
    private LayerMask m_openMask;

    private PlayerData m_data;

    void Awake()
    {
        m_data = GameObject.FindGameObjectsWithTag("Player")[0].GetComponent<PlayerData>();
    }

    void Start()
    {
        m_openText.SetActive(false);
    }

    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, m_openDistance, m_openMask))
        {
            if (Input.GetKeyDown(keys.grabKey) && m_data.hasKey)
            {
                DoorController door = hit.transform.parent.gameObject.GetComponent<DoorController>();
                door.openDoor();
                Debug.Log("open");
            }
            m_openText.SetActive(true);
        }
        else
        {
            m_openText.SetActive(false);
        }
    }
}
