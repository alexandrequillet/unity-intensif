﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerSO", menuName = "SO/PlayerSO", order = 3)]
public class PlayerSO : ScriptableObject
{
    public float speed;
    public float fearSpeed;
    public float grabDistance;
}
