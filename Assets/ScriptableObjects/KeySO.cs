﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "KeySO", menuName = "SO/KeySO", order = 1)]
public class KeySO : ScriptableObject
{
    public KeyCode moveForwardKey = KeyCode.Z;
    public KeyCode moveBackwardKey = KeyCode.S;
    public KeyCode moveLeftKey = KeyCode.Q;
    public KeyCode moveRightKey = KeyCode.D;
    public KeyCode jumpKey = KeyCode.Space;
    public KeyCode pauseKey = KeyCode.Escape;
    public KeyCode grabKey = KeyCode.G;
    public KeyCode openKey = KeyCode.O;
}
