﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GrabbableObjectSO", menuName = "SO/GrabbableObjectSO", order = 2)]
public class GrabbableObjectSO : ScriptableObject
{
    public int removeFear;
    public int addScore;
}
