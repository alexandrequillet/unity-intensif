README

Escape Fear est un jeu d enigme qui se deroule dans un donjon.  
Vous endossez le role du personnage qui doit sortir de ce donjon. Pour cela, vous devez resoudre des enigmes pour sortir de ce lieu. Dans cet effrayant donjon, plus le temps passe et plus votre niveau de peur augmente. Plus vous avez peur, moins votre zone de vision est grande. Cela vous handicape pour trouver les enigmes/puzzle et les reussir. Le fait de resoudre des enigmes fait diminuer votre niveau de peur car vous serez content d avoir reussir, cela vous encourage et vous rassure. Dans la salle, des objets sont dissimules et vous permet d augmenter le score et baisser le niveau de peur.

Parametres:
Z : avance
Q : gauche
S : recule
D : droite
G : ramasse 
P : menu pause


Astuce : Recueillir une clé et allez vers la porte finale



Developpeurs : Emilie Banh, Alexandre Quillet, Olivier Rotger, Benjamin Vignaux